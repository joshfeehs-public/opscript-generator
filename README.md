# Opscript Generator
When I'm doing red teaming, I really like to copy-paste from a pre-planned opscript as much as I can. It makes me a lot more likely to do things correctly, with good opsec, and not forget steps.
This repo is a rough poc for automatically taking an opscript template and creating specific opscripts per user. This way, I can start with a template and then find and replace my way to individualized opscripts for each different machine I am using.
