#!/usr/bin/env python3
import argparse
import json
import os
from datetime import datetime
import shutil


def configure_directory(dir, jsonfile,outdir):
    ''' Parses a directory and calls configure_file() for each opscript in the dir'''
    # We already have tested that it's a directory
    dirlist = os.listdir(dir)
    print(dirlist)
    for opscript in dirlist:
        configure_file(dir + opscript,jsonfile,outdir)

def configure_file(opscript,jsonfile,outdir):
    ''' Actually parses JSON and generates the opscript'''
    f = open(jsonfile)
    config = json.load(f)
    f.close()
    for target in config:
        # Construct output filename out of a string that may or may not be a full path to the file
        opscriptname = opscript.split("/")[-1]
        outfilename = '.'.join(opscriptname.split(".")[:-1]) + "_" + target + "_" + datetime.today().strftime('%Y%m%d') + ".script"
        print(f"[+] Creating copy of {opscriptname} for target {target}")

        opscript_contents = open(opscript,"r").read()
        for var in config[target]:
            print(f"[*] Replacing variable: {var}")
            opscript_contents = opscript_contents.replace(var,config[target][var])
        o = open(outdir + outfilename,"w")
        o.write(opscript_contents)
        o.close()
    

def main():
    '''
    This reads in the CLI arguments and passes them to the main worker functions.
    It also handles setting up some of the files/directories that will be needed.
    '''
    parser = argparse.ArgumentParser()
    parser.add_argument("-i","--input",help="The file or folder containing files that you want to modify.",required=True)    
    parser.add_argument("-j","--json",help="The JSON config file to use",required=True)
    parser.add_argument("-o","--outdir",help="The directory to write the opscripts to",default="./")
    args = parser.parse_args()

    # Make the output directory if it doesn't already exist
    if not os.path.exists(args.outdir):
        os.makedirs(args.outdir)

    # Figure out if we're parsing one file or a folder of files, then call the correct parsing function
    if os.path.isdir(args.input):
        configure_directory(args.input,args.json,args.outdir)
    elif os.path.isfile(args.input):
        configure_file(args.input,args.json,args.outdir)
    else:
        print(f"Error, path {args.input} is not a file or directory.")

    # Now that the opscripts are generated, copy the config into the output directory for safekeeping
    jsonfile = args.json.split("/")[-1]
    jsoncopy = args.outdir.rstrip("/") + "/" + jsonfile
    shutil.copy(args.json,jsoncopy)

if __name__ == "__main__":
    main()
